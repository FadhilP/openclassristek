﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed = 5f;
    public float cooldown = 0.5f;
    public GameObject proj;

    void Update()
    {
        //Movement
        float horizontal = Input.GetAxis("Horizontal");
        transform.position += new Vector3(horizontal, 0, 0) * speed * Time.deltaTime;

        //Shoot projectile
        if (Input.GetKeyDown("space") && Time.time >= cooldown)
        {
            cooldown += 0.5f;
            Instantiate(proj, transform.position + Vector3.up, transform.rotation);
        }
    }
}
