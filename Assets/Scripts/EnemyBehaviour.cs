﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBehaviour : MonoBehaviour
{
    private Transform enemyHolder;
    public float speed;

    private Text text;

    void Start()
    {
        text = GameObject.Find("EndText").GetComponent<Text>();
        enemyHolder = GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        enemyHolder.position += Vector3.right * speed * Time.deltaTime;

        foreach (Transform enemy in enemyHolder)
        {
            if (enemy.position.x < -10.5 || enemy.position.x > 10.5)
            {
                speed = -speed;
                enemyHolder.position += Vector3.down * 0.5f;
                return;
            }

            if (enemy.position.y <= -1.5f)
            {
                Time.timeScale = 0;
                text.text = "You Lose! press R to restart";

            }
        }

        if (enemyHolder.childCount == 0)
        {
            text.text = "You Win! press R to restart";
            Time.timeScale = 0;
        }
    }
}
